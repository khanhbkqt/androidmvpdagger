package com.kasoft.mvpdaggerexample.module;

import com.kasoft.mvpdaggerexample.data.DataSource;
import com.kasoft.mvpdaggerexample.data.RemoteDataSource;
import com.kasoft.mvpdaggerexample.data.retrofit.AppAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by khanhnguyen on 14/04/2017
 */

@Module
public class DataSourceModule {

    @Provides @Singleton
    DataSource provideDataSource(AppAPI appAPI) {
        return new RemoteDataSource(appAPI);
    }
}
