package com.kasoft.mvpdaggerexample.ui.main;

import com.kasoft.mvpdaggerexample.base.BasePresenter;
import com.kasoft.mvpdaggerexample.base.BaseState;
import com.kasoft.mvpdaggerexample.base.BaseView;
import com.kasoft.mvpdaggerexample.data.bean.Post;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public interface MainContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<Presenter> {
        void setLoadingIndicator(boolean enable);
        void showToast(String message);

        void displayPost(Post post);
    }

    interface State extends BaseState {

    }
}
