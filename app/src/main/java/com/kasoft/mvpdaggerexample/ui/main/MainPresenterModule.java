package com.kasoft.mvpdaggerexample.ui.main;

import dagger.Module;
import dagger.Provides;

/**
 * Created by khanhnguyen on 14/04/2017
 */

@Module
public class MainPresenterModule {

    private final MainContract.View mView;

    public MainPresenterModule(MainContract.View mView) {
        this.mView = mView;
    }

    @Provides
    MainContract.View provideMainContractView() {
        return mView;
    }
}
