package com.kasoft.mvpdaggerexample.ui.main;

import android.support.annotation.Nullable;

import com.kasoft.mvpdaggerexample.base.BaseState;
import com.kasoft.mvpdaggerexample.data.DataSource;
import com.kasoft.mvpdaggerexample.data.bean.Post;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public class MainPresenter implements MainContract.Presenter {

    private Subscription mRequestSubscription;

    private MainContract.View mView;
    private DataSource mDataSource;

    @Inject
    public MainPresenter(MainContract.View view, DataSource dataSource) {
        this.mView = view;
        this.mDataSource = dataSource;
    }

    @Inject
    void setup() {
        mView.setPresenter(this);
    }

    @Override
    public void start(@Nullable BaseState state) {
        mView.setLoadingIndicator(true);

        mRequestSubscription = mDataSource.getPost("123")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Post>() {
                    @Override
                    public void call(Post post) {
                        mView.setLoadingIndicator(false);

                        mView.displayPost(post);

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mView.setLoadingIndicator(false);
                        mView.showToast(throwable.getMessage());
                    }
                });
    }

    @Override
    public void stop() {
        if (!mRequestSubscription.isUnsubscribed())
            mRequestSubscription.unsubscribe();
    }

    @Override
    public BaseState getState() {
        return null;
    }
}
