package com.kasoft.mvpdaggerexample.ui.main;

import com.kasoft.mvpdaggerexample.MainActivity;
import com.kasoft.mvpdaggerexample.interfaces.FragmentScope;
import com.kasoft.mvpdaggerexample.module.DataSourceComponent;

import dagger.Component;

/**
 * Created by khanhnguyen on 14/04/2017
 */
@FragmentScope
@Component(dependencies = DataSourceComponent.class, modules = MainPresenterModule.class)
public interface MainComponent {
    void inject(MainActivity activity);
}
