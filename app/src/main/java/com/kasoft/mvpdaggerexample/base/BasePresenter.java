package com.kasoft.mvpdaggerexample.base;

import android.support.annotation.Nullable;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public interface BasePresenter {
    void start(@Nullable BaseState state);
    void stop();
    BaseState getState();
}
