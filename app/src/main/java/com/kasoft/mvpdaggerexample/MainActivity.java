package com.kasoft.mvpdaggerexample;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.kasoft.mvpdaggerexample.data.bean.Post;
import com.kasoft.mvpdaggerexample.ui.main.DaggerMainComponent;
import com.kasoft.mvpdaggerexample.ui.main.MainContract;
import com.kasoft.mvpdaggerexample.ui.main.MainPresenter;
import com.kasoft.mvpdaggerexample.ui.main.MainPresenterModule;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private ProgressDialog mProgressDialog;

    @Inject
    MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading...");

        DaggerMainComponent.builder()
                .dataSourceComponent(((App) getApplication()).getDataSourceComponent())
                .mainPresenterModule(new MainPresenterModule(this))
                .build().inject(this);
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        this.mMainPresenter = (MainPresenter) presenter;
        mMainPresenter.start(null);
    }

    @Override
    public void setLoadingIndicator(boolean enable) {
        if (enable) {
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayPost(Post post) {
        showToast(post.toString());
    }
}
