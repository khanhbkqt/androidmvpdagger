package com.kasoft.mvpdaggerexample;

import android.app.Application;

import com.kasoft.mvpdaggerexample.module.AppModule;
import com.kasoft.mvpdaggerexample.module.DaggerDataSourceComponent;
import com.kasoft.mvpdaggerexample.module.DaggerNetComponent;
import com.kasoft.mvpdaggerexample.module.DataSourceComponent;
import com.kasoft.mvpdaggerexample.module.DataSourceModule;
import com.kasoft.mvpdaggerexample.module.NetComponent;
import com.kasoft.mvpdaggerexample.module.NetModule;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public class App extends Application {

    private NetComponent mNetComponent;
    private DataSourceComponent mDataSourceComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("https://google.com/"))
                .build();

        mDataSourceComponent = DaggerDataSourceComponent.builder()
                .netComponent(mNetComponent)
                .dataSourceModule(new DataSourceModule())
                .build();
    }

    public DataSourceComponent getDataSourceComponent() {
        return mDataSourceComponent;
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}
