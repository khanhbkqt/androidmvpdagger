package com.kasoft.mvpdaggerexample.data;

import com.kasoft.mvpdaggerexample.data.bean.Post;

import java.util.List;

import rx.Observable;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public interface DataSource {
    Observable<List<Post>> getPostList(long date);

    Observable<Post> getPost(String postId);
}
