package com.kasoft.mvpdaggerexample.data;

import com.kasoft.mvpdaggerexample.data.bean.Post;
import com.kasoft.mvpdaggerexample.data.retrofit.AppAPI;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public class RemoteDataSource implements DataSource {

    private AppAPI mApi;

    @Inject
    public RemoteDataSource(AppAPI api) {
        this.mApi = api;
    }

    @Override
    public Observable<List<Post>> getPostList(long date) {
        return mApi.getListPost(date);
    }

    @Override
    public Observable<Post> getPost(String postId) {
        return mApi.getPost(postId);
    }
}
