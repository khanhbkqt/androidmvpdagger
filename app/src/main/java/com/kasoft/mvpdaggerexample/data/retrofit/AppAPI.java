package com.kasoft.mvpdaggerexample.data.retrofit;

import com.kasoft.mvpdaggerexample.data.bean.Post;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public interface AppAPI {
    @GET(Endpoints.GET_POST_LIST)
    Observable<List<Post>> getListPost(@Header("post_last_date") long date);
    @GET(Endpoints.GET_POST)
    Observable<Post> getPost(@Header("post_id") String postId);
}
