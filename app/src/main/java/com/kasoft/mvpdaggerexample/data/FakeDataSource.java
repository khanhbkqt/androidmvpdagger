package com.kasoft.mvpdaggerexample.data;

import android.content.Context;

import com.kasoft.mvpdaggerexample.data.bean.Post;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by khanhnguyen on 14/04/2017
 */

public class FakeDataSource implements DataSource {

    private Context context;

    @Inject
    public FakeDataSource(Context context) {
        this.context = context;
    }

    @Override
    public Observable<List<Post>> getPostList(long date) {
        return Observable.fromCallable(new Callable<List<Post>>() {
            @Override
            public List<Post> call() throws Exception {
                Thread.sleep(5000);
                return new ArrayList<>();
            }
        });
    }

    @Override
    public Observable<Post> getPost(String postId) {
        return Observable.fromCallable(new Callable<Post>() {
            @Override
            public Post call() throws Exception {
                Thread.sleep(5000);
                return new Post();
            }
        });
    }
}
