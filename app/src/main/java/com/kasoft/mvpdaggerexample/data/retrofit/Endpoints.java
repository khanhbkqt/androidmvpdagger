package com.kasoft.mvpdaggerexample.data.retrofit;

/**
 * Created by khanhnguyen on 14/04/2017
 */

class Endpoints {
    static final String GET_POST_LIST = "posts/get";
    static final String GET_POST = "?q=test";
}
