package com.kasoft.mvpdaggerexample.module;

import android.content.Context;

import com.kasoft.mvpdaggerexample.data.retrofit.AppAPI;

import dagger.Component;

/**
 * Created by khanhnguyen on 14/04/2017
 */
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    AppAPI provideAppAPI();
    Context provideContext();
}
