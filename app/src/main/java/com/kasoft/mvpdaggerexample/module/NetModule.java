package com.kasoft.mvpdaggerexample.module;

import com.google.gson.Gson;
import com.kasoft.mvpdaggerexample.data.DataSource;
import com.kasoft.mvpdaggerexample.data.FakeDataSource;
import com.kasoft.mvpdaggerexample.data.RemoteDataSource;
import com.kasoft.mvpdaggerexample.data.retrofit.AppAPI;
import com.kasoft.mvpdaggerexample.interfaces.ActivityScope;
import com.kasoft.mvpdaggerexample.interfaces.Fake;
import com.kasoft.mvpdaggerexample.interfaces.Remote;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by khanhnguyen on 14/04/2017
 */

@Module
public class NetModule {

    private String mBaseUrl;

    public NetModule(String baseUrl) {
        this.mBaseUrl = baseUrl;
    }

    @Provides
    public String provideBaseUrl() {
        return mBaseUrl;
    }

    @Provides
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    public Retrofit provideRetrofit(String baseUrl, Gson gson) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl)
                .build();
    }

    @Provides
    public AppAPI provideAppAPI(Retrofit retrofit) {
        AppAPI api = retrofit.create(AppAPI.class);
        return api;
    }
}
