package com.kasoft.mvpdaggerexample.module;

import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;

/**
 * Created by khanhnguyen on 14/04/2017
 */

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context.getApplicationContext();
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Provides
    public SharedPreferences provideSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("APP", Context.MODE_PRIVATE);
        return sharedPreferences;
    }
}
