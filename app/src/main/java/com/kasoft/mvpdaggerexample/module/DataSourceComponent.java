package com.kasoft.mvpdaggerexample.module;

import com.kasoft.mvpdaggerexample.data.DataSource;
import com.kasoft.mvpdaggerexample.interfaces.ActivityScope;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by khanhnguyen on 14/04/2017
 */

@Singleton
@Component(dependencies = NetComponent.class, modules = {DataSourceModule.class})
public interface DataSourceComponent {
    DataSource getDataSource();
}
